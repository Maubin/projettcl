# Projet de compilation

Ce projet sert à créer un compilateur pour le langage NILNOVI PROCÉDURAL (NNP). 
Il est donc d'abord question de compiler le programme contenu dans un fichier .nno :
````bash 
python anasyn.py input.nno -o output.txt
````
Il faut ensuite exécuter le programme avec la commande :
````bash
python vm.py output.txt
````
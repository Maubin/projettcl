#!/usr/bin/python

##      @package anasyn
#       Syntactical Analyser package. 
#

import sys, argparse, re
import logging

import analex

logger = logging.getLogger('anasyn')

DEBUG = False
LOGGING_LEVEL = logging.DEBUG


class AnaSynException(Exception):
        def __init__(self, value):
                self.value = value
        def __str__(self):
                return repr(self.value)

from copy import deepcopy

identifierTable = []              
currentProcedureIndex = 0

def retrieve(lex):
        return lex.lexical_units[lex.lexical_unit_index-1].value

def fillIdentType(Type,mod = None):
        i = len(identifierTable)-1
        while identifierTable[i][1]=="ident" and len(identifierTable[i])<4:
                identifierTable[i].append(Type)
                if mod is None:
                        identifierTable[i].append('')
                else:
                        identifierTable[i].append(mod)
                        identifierTable[currentProcedureIndex][4].append(mod)
                i-=1

def setIdentType(ident, Type):
        for i in range(len(identifierTable)):
                if identifierTable[i][0] == ident:
                        if len(identifierTable[i]) >= 4:
                                identifierTable[i][3] = Type
                        else:
                                identifierTable[i].append(Type)
                        break

def getIdentType(ident):
        start = 0 if isFonc(ident) else currentProcedureIndex
        for i in range(start,len(identifierTable)):
                if identifierTable[i][0] == ident:
                        return identifierTable[i][3]

def isFonc(ident):
        for i in range(len(identifierTable)):
                if identifierTable[i][0] == ident:
                        return identifierTable[i][1] == "function"

def isIdent(ident):
        start = 0 if isFonc(ident) else currentProcedureIndex
        for i in range(start,len(identifierTable)):
                if identifierTable[i][0] == ident:
                        return True
        return False

def currentFonc():
        return identifierTable[currentProcedureIndex]

def getTableIndex(ident):
        start = 0 if isFonc(ident) else currentProcedureIndex
        for i in range(start,len(identifierTable)):
                if identifierTable[i][0] == ident:
                        return i
        msg = ident+"' is not defined"
        logger.error(msg)
        raise AnaSynException(msg) 

def getCall(ident):
        adr = getTableIndex(ident)
        if identifierTable[adr][4] == "in out":
                return "empilerParam("+str(identifierTable[adr][2])+")"
        elif currentProcedureIndex == 0:
                return "empiler("+str(identifierTable[adr][2])+")"
        else:
                return "empilerAd("+str(identifierTable[adr][2])+")"
                



########################################################################                                        
#### Syntactical Diagrams
########################################################################                                        

def program(lexical_analyser):
        specifProgPrinc(lexical_analyser)
        lexical_analyser.acceptKeyword("is")
        corpsProgPrinc(lexical_analyser)
        codeGenerator.code.append("finProg()")
        
def specifProgPrinc(lexical_analyser):
        lexical_analyser.acceptKeyword("procedure")
        ident = lexical_analyser.acceptIdentifier()
        
        identifierTable.append([ident,"procedure",0,"void",[]])
        codeGenerator.code.append("debutProg()")
                
        logger.debug("Name of program : "+ident)
        
def  corpsProgPrinc(lexical_analyser):
        if not lexical_analyser.isKeyword("begin"):
                logger.debug("Parsing declarations")
                partieDecla(lexical_analyser)
                logger.debug("End of declarations")
        lexical_analyser.acceptKeyword("begin")
        
        if not lexical_analyser.isKeyword("end"):
                logger.debug("Parsing instructions")
                suiteInstr(lexical_analyser)
                logger.debug("End of instructions")
        
        lexical_analyser.acceptKeyword("end")
        lexical_analyser.acceptFel()
        logger.debug("End of program")
        
def partieDecla(lexical_analyser):
        if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword("function") :
                listeDeclaOp(lexical_analyser)
                if not lexical_analyser.isKeyword("begin"):
                        nbP = listeDeclaVar(lexical_analyser)
                        codeGenerator.code.append("reserver("+str(nbP)+")")
        else:
                nbP = listeDeclaVar(lexical_analyser)
                codeGenerator.code.append("reserver("+str(nbP)+")")            

def listeDeclaOp(lexical_analyser):
        declaOp(lexical_analyser)
        lexical_analyser.acceptCharacter(";")
        if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword("function") :
                listeDeclaOp(lexical_analyser)

def declaOp(lexical_analyser):
        if lexical_analyser.isKeyword("procedure"):
                procedure(lexical_analyser)
        if lexical_analyser.isKeyword("function"):
                fonction(lexical_analyser)

def procedure(lexical_analyser):
        global currentProcedureIndex

        lexical_analyser.acceptKeyword("procedure")
        ident = lexical_analyser.acceptIdentifier()

        i = len(identifierTable)
        tra = len(codeGenerator.code)
        codeGenerator.code.append("tra")
        identifierTable.append([ident,"procedure",len(codeGenerator.code),"void",[]])

        currentProcedureIndexbuf = currentProcedureIndex
        currentProcedureIndex = i

        logger.debug("Name of procedure : "+ident)
       
        partieFormelle(lexical_analyser)
        
        lexical_analyser.acceptKeyword("is")
        corpsProc(lexical_analyser)
        currentProcedureIndex = currentProcedureIndexbuf
        codeGenerator.code.append("retourProc()")
        codeGenerator.code[tra] = "tra("+str(len(codeGenerator.code))+")"

       

def fonction(lexical_analyser):
        global currentProcedureIndex

        lexical_analyser.acceptKeyword("function")
        ident = lexical_analyser.acceptIdentifier()
        logger.debug("Name of function : "+ident)

        i = len(identifierTable)
        tra = len(codeGenerator.code)
        codeGenerator.code.append("tra")
        identifierTable.append([ident,"function",len(codeGenerator.code),"<return_type>",[]])
        
        currentProcedureIndexbuf = currentProcedureIndex
        currentProcedureIndex = i
        partieFormelle(lexical_analyser)

        lexical_analyser.acceptKeyword("return")
        
        nnpType(lexical_analyser)
        Type = retrieve(lexical_analyser)
        setIdentType(ident, Type)

        lexical_analyser.acceptKeyword("is")

        corpsFonct(lexical_analyser)
        currentProcedureIndex = currentProcedureIndexbuf
        codeGenerator.code[tra] = "tra("+str(len(codeGenerator.code))+")"

def corpsProc(lexical_analyser):
        if not lexical_analyser.isKeyword("begin"):
                partieDeclaProc(lexical_analyser)
        lexical_analyser.acceptKeyword("begin")
        suiteInstr(lexical_analyser)
        lexical_analyser.acceptKeyword("end")

def corpsFonct(lexical_analyser):
        if not lexical_analyser.isKeyword("begin"):
                partieDeclaProc(lexical_analyser)
        lexical_analyser.acceptKeyword("begin")
        suiteInstrNonVide(lexical_analyser)
        lexical_analyser.acceptKeyword("end")

def partieFormelle(lexical_analyser):
        lexical_analyser.acceptCharacter("(")
        if not lexical_analyser.isCharacter(")"):
                listeSpecifFormelles(lexical_analyser)
        lexical_analyser.acceptCharacter(")")

def listeSpecifFormelles(lexical_analyser,n=0):
        nbP = specif(lexical_analyser,n)
        if not lexical_analyser.isCharacter(")"):
                lexical_analyser.acceptCharacter(";")
                listeSpecifFormelles(lexical_analyser,n+nbP)

def specif(lexical_analyser,n):
        nbP = listeIdent(lexical_analyser,True,n)

        lexical_analyser.acceptCharacter(":")
        if lexical_analyser.isKeyword("in"):
                mod = mode(lexical_analyser)
        else:
                mod = ''
                
        nnpType(lexical_analyser)
        Type = retrieve(lexical_analyser)
        fillIdentType(Type,mod)
        return nbP

def mode(lexical_analyser):
        lexical_analyser.acceptKeyword("in")
        if lexical_analyser.isKeyword("out"):
                lexical_analyser.acceptKeyword("out")
                logger.debug("in out parameter")
                return "in out"  
        else:
                logger.debug("in parameter")
                return "in"

def nnpType(lexical_analyser):
        if lexical_analyser.isKeyword("integer"):
                lexical_analyser.acceptKeyword("integer")
                logger.debug("integer type")
           
        elif lexical_analyser.isKeyword("boolean"):
                lexical_analyser.acceptKeyword("boolean")
                logger.debug("boolean type")

        else:
                logger.error("Unknown type found <"+ lexical_analyser.get_value() +">!")
                raise AnaSynException("Unknown type found <"+ lexical_analyser.get_value() +">!")

def partieDeclaProc(lexical_analyser):
        nbP = listeDeclaVar(lexical_analyser)
        codeGenerator.code.append("reserver("+str(nbP)+")")

def listeDeclaVar(lexical_analyser,n=0):
        nbP = declaVar(lexical_analyser,n)
        if lexical_analyser.isIdentifier():
                nbP += listeDeclaVar(lexical_analyser,n+nbP)
        return nbP

def declaVar(lexical_analyser,n):
        nbP = listeIdent(lexical_analyser,False,n)
        lexical_analyser.acceptCharacter(":")
        logger.debug("now parsing type...")


        nnpType(lexical_analyser)
        Type = retrieve(lexical_analyser)
        fillIdentType(Type)
        
        lexical_analyser.acceptCharacter(";")
        return nbP

def listeIdent(lexical_analyser,args = False,nP = 0):
        ident = lexical_analyser.acceptIdentifier()
        if ident:
                
                if isIdent(ident):
                        msg = "Identifier '"+ident+"' is already used!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg) 
                
                identifierTable.append([ident,"ident",nP])
                
        logger.debug("identifier found: "+str(ident))

        if lexical_analyser.isCharacter(","):
                lexical_analyser.acceptCharacter(",")
                return 1 + listeIdent(lexical_analyser,args,nP+1)
        return 1

def suiteInstrNonVide(lexical_analyser):
        instr(lexical_analyser)
        if lexical_analyser.isCharacter(";"):
                lexical_analyser.acceptCharacter(";")
                suiteInstrNonVide(lexical_analyser)

def suiteInstr(lexical_analyser):
        if not lexical_analyser.isKeyword("end"):
                suiteInstrNonVide(lexical_analyser)

def instr(lexical_analyser):            
        if lexical_analyser.isKeyword("while"):
                boucle(lexical_analyser)
        elif lexical_analyser.isKeyword("if"):
                altern(lexical_analyser)
        elif lexical_analyser.isKeyword("get") or lexical_analyser.isKeyword("put"):
                es(lexical_analyser)
        elif lexical_analyser.isKeyword("return"):
                expreType = retour(lexical_analyser)
                fonc = currentFonc()
                if fonc[3] != expreType:
                        msg = "Expecting a "+fonc[3]+" value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg) 
                
        elif lexical_analyser.isIdentifier():
                ident = lexical_analyser.acceptIdentifier()
                Type = getIdentType(ident)
                adr = getTableIndex(ident)
                if lexical_analyser.isSymbol(":="):                             
                        # affectation
                        lexical_analyser.acceptSymbol(":=")

                        codeGenerator.code.append(getCall(ident))
                        expreType = expression(lexical_analyser)

                        if expreType != Type:
                                msg = "Expecting a "+Type+" value!"
                                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                logger.error(msg)
                                raise AnaSynException(msg) 

                        codeGenerator.code.append("affectation()")
                        
                        logger.debug("parsed affectation")

                elif lexical_analyser.isCharacter("("):     
                        codeGenerator.code.append("reserverBloc()")              

                        lexical_analyser.acceptCharacter("(")
                        if not lexical_analyser.isCharacter(")"):
                                listePe(lexical_analyser,adr)

                        lexical_analyser.acceptCharacter(")")

                        line = identifierTable[adr][2]
                        nparam = len(identifierTable[adr][4])
                        codeGenerator.code.append("traStat("+str(line)+","+str(nparam)+")")

                        logger.debug("parsed procedure call")
                else:
                        logger.error("Expecting procedure call or affectation!")
                        raise AnaSynException("Expecting procedure call or affectation!")
                
        else:
                logger.error("Unknown Instruction <"+ lexical_analyser.get_value() +">!")
                raise AnaSynException("Unknown Instruction <"+ lexical_analyser.get_value() +">!")

def listePe(lexical_analyser,adr,nP=0):
        expression(lexical_analyser)
        if len(identifierTable[adr][4]) and identifierTable[adr][4][nP]=="in out":
                codeGenerator.code.pop(-1)
        if lexical_analyser.isCharacter(","):
                lexical_analyser.acceptCharacter(",")
                listePe(lexical_analyser,adr,nP+1)


def expression(lexical_analyser): 
        logger.debug("parsing expression: " + str(lexical_analyser.get_value()))
        
        expreType = exp1(lexical_analyser)
        while lexical_analyser.isKeyword("or"):
                if expreType == "boolean":
                        lexical_analyser.acceptKeyword("or")
                        expreType = exp1(lexical_analyser)
                        if expreType == "boolean":
                                codeGenerator.code.append(("ou()"))
                                return "boolean"
                        else:
                                msg = "Expecting a boolean value!"
                                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                logger.error(msg)
                                raise AnaSynException(msg)   
                else:
                        msg = "Expecting a boolean value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg)   
        return expreType


def exp1(lexical_analyser):
        logger.debug("parsing exp1")
        
        expreType = exp2(lexical_analyser)
        while lexical_analyser.isKeyword("and"):

                if expreType == "boolean":
                        lexical_analyser.acceptKeyword("and")
                        expreType = exp2(lexical_analyser)

                        if expreType == "boolean":		
                                codeGenerator.code.append(("et()"))
                        else:
                                msg = "Expecting a boolean value!"
                                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                logger.error(msg)
                                raise AnaSynException(msg)   
                else:
                        msg = "Expecting a boolean value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg)   
        return expreType

def exp2(lexical_analyser):
        logger.debug("parsing exp2")

        expreType = exp3(lexical_analyser)
              
        if lexical_analyser.isSymbol("=") or \
                lexical_analyser.isSymbol("/="): 
                opp = lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].value

                if expreType in ['integer',"boolean"]:
                        opRel(lexical_analyser)
                        expreType = exp3(lexical_analyser)
                        if expreType in ['integer',"boolean"]:
                                codeGenerator.code.append("egal()" if opp=="=" else "diff()")
                                return "boolean"
                        else:
                                msg = "Expecting an integer or boolean value!"
                                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                logger.error(msg)
                                raise AnaSynException(msg)   
                else:
                        msg ="Expecting an integer or boolean value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg)  
                   
        elif  (lexical_analyser.isSymbol("<") or \
                        lexical_analyser.isSymbol("<=") or \
                        lexical_analyser.isSymbol(">") or \
                        lexical_analyser.isSymbol(">=")):
                        opp = lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].value

                        if expreType == 'integer':
                                opRel(lexical_analyser)
                                expreType = exp3(lexical_analyser)
                                if expreType == "integer":
                                        codeGenerator.code.append("infeg()" if opp=="<=" else "inf()" if opp=="<" else "supeg()" if opp==">=" else "sup()")
                                        return "boolean"
                                else:
                                        msg = "Expecting an integer value!"
                                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                        logger.error(msg)
                                        raise AnaSynException(msg)   
                        else:
                                msg = "Expecting an integer value!"
                                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                logger.error(msg)
                                raise AnaSynException(msg)   
        return expreType               
        
def opRel(lexical_analyser):
        logger.debug("parsing relationnal operator: " + lexical_analyser.get_value()) 
        
        if      lexical_analyser.isSymbol("<"):
                lexical_analyser.acceptSymbol("<")
        
        elif lexical_analyser.isSymbol("<="):
                lexical_analyser.acceptSymbol("<=")
        
        elif lexical_analyser.isSymbol(">"):
                lexical_analyser.acceptSymbol(">")
        
        elif lexical_analyser.isSymbol(">="):
                lexical_analyser.acceptSymbol(">=")
        
        elif lexical_analyser.isSymbol("="):
                lexical_analyser.acceptSymbol("=")
        
        elif lexical_analyser.isSymbol("/="):
                lexical_analyser.acceptSymbol("/=")
        
        else:
                msg = "Unknown relationnal operator <"+ lexical_analyser.get_value() +">!"
                logger.error(msg)
                raise AnaSynException(msg)

def exp3(lexical_analyser):
        logger.debug("parsing exp3")
        expreType = exp4(lexical_analyser)

        while lexical_analyser.isCharacter("+") or lexical_analyser.isCharacter("-"):

                opp = lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].value

                if expreType == "integer":
                        opAdd(lexical_analyser)
                        expreType = exp4(lexical_analyser)
                        
                        if expreType == "integer":
                                codeGenerator.code.append("add()" if opp=="+" else "sous()")
                        else:
                                msg = "Expecting a integer value!"
                                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                logger.error(msg)
                                raise AnaSynException(msg) 
                else:
                        msg = "Expecting a integer value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg)   
        return expreType

def opAdd(lexical_analyser):
        logger.debug("parsing additive operator: " + lexical_analyser.get_value())
        if lexical_analyser.isCharacter("+"):
                lexical_analyser.acceptCharacter("+")
                
        elif lexical_analyser.isCharacter("-"):
                lexical_analyser.acceptCharacter("-")
                
        else:
                msg = "Unknown additive operator <"+ lexical_analyser.get_value() +">!"
                logger.error(msg)
                raise AnaSynException(msg)

def exp4(lexical_analyser):
        logger.debug("parsing exp4")
        
        expreType = prim(lexical_analyser) 

        while lexical_analyser.isCharacter("*") or lexical_analyser.isCharacter("/"):
                
                if expreType == "integer":
                        
                        opp = opMult(lexical_analyser)
                        expreType = prim(lexical_analyser)
                        
                        if expreType == "integer":
                                codeGenerator.code.append(opp)
                        else:
                                msg = "Expecting a integer value!"
                                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                                logger.error(msg)
                                raise AnaSynException(msg) 
                else:
                        msg = "Expecting a integer value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg) 
        return expreType

def opMult(lexical_analyser):
        logger.debug("parsing multiplicative operator: " + lexical_analyser.get_value())
        if lexical_analyser.isCharacter("*"):
                lexical_analyser.acceptCharacter("*")
                return "mult()"
                
        elif lexical_analyser.isCharacter("/"):
                lexical_analyser.acceptCharacter("/")
                return "div()"
                
        else:
                msg = "Unknown multiplicative operator <"+ lexical_analyser.get_value() +">!"
                logger.error(msg)
                raise AnaSynException(msg)

def prim(lexical_analyser):
        logger.debug("parsing prim")
        
        if lexical_analyser.isCharacter("+") or lexical_analyser.isCharacter("-") or lexical_analyser.isKeyword("not"):

                call = opUnaire(lexical_analyser)
                expreType = elemPrim(lexical_analyser)
                codeGenerator.code.append(call)
                return expreType
        else:
                return elemPrim(lexical_analyser)

def opUnaire(lexical_analyser):
        logger.debug("parsing unary operator: " + lexical_analyser.get_value())
        if lexical_analyser.isCharacter("+"):
                lexical_analyser.acceptCharacter("+")
                return "()"
        elif lexical_analyser.isCharacter("-"):
                lexical_analyser.acceptCharacter("-")
                return "moins()"
        elif lexical_analyser.isKeyword("not"):
                lexical_analyser.acceptKeyword("not")
                return "non()"
        else:
                msg = "Unknown additive operator <"+ lexical_analyser.get_value() +">!"
                logger.error(msg)
                raise AnaSynException(msg)

def elemPrim(lexical_analyser):
        logger.debug("parsing elemPrim: " + str(lexical_analyser.get_value()))
        if lexical_analyser.isCharacter("("):
                lexical_analyser.acceptCharacter("(")
                expreType = expression(lexical_analyser)
                lexical_analyser.acceptCharacter(")")
                return expreType
   
        elif lexical_analyser.isInteger() or lexical_analyser.isKeyword("true") or lexical_analyser.isKeyword("false"):
                expreType = valeur(lexical_analyser)
                return expreType

        elif lexical_analyser.isIdentifier():
                ident = lexical_analyser.acceptIdentifier()
                expreType = getIdentType(ident)

                if expreType is None:
                        logger.error("Unknown Value!")
                        raise AnaSynException("Unknown Value! : '" +ident+ "' ")

                if not isFonc(ident):
                        codeGenerator.code.append(getCall(ident))
                        codeGenerator.code.append("valeurPile()")
                        return expreType
                
                elif lexical_analyser.isCharacter("("):       
                        
                        codeGenerator.code.append("reserverBloc()")

                        lexical_analyser.acceptCharacter("(")
                        if not lexical_analyser.isCharacter(")"):
                                listePe(lexical_analyser,getTableIndex(ident))
                        
                        lexical_analyser.acceptCharacter(")")

                        line = identifierTable[getTableIndex(ident)][2]
                        nparam = len(identifierTable[getTableIndex(ident)][4])
                        codeGenerator.code.append("traStat("+str(line)+","+str(nparam)+")")

                        logger.debug("parsed procedure call")

                        logger.debug("Call to function: " + ident)

                        return expreType
                          
                else:
                        logger.debug("Use of an identifier as an expression: " + ident)
                        msg = "Use of an identifier as an expression: " + ident
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg) 
                
        else:
                msg = "Unknown Value!"
                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                logger.error(msg)
                raise AnaSynException(msg) 

def valeur(lexical_analyser):
        if lexical_analyser.isInteger():
                entier = lexical_analyser.acceptInteger()
                logger.debug("integer value: " + str(entier))

                codeGenerator.code.append("empiler("+str(entier)+")")
                return "integer"
        elif lexical_analyser.isKeyword("true") or lexical_analyser.isKeyword("false"):
                vtype = valBool(lexical_analyser)
                return vtype
        else:
                logger.error("Unknown Value! Expecting an integer or a boolean value!")
                raise AnaSynException("Unknown Value ! Expecting an integer or a boolean value!")

def valBool(lexical_analyser):
        if lexical_analyser.isKeyword("true"):
                lexical_analyser.acceptKeyword("true")  
                logger.debug("boolean true value")
                codeGenerator.code.append("empiler(1)")
        else:
                logger.debug("boolean false value")
                lexical_analyser.acceptKeyword("false")
                codeGenerator.code.append("empiler(0)")
        
        return "boolean"

def es(lexical_analyser):
        logger.debug("parsing E/S instruction: " + lexical_analyser.get_value())
        if lexical_analyser.isKeyword("get"):
                lexical_analyser.acceptKeyword("get")
                lexical_analyser.acceptCharacter("(")

                ident = lexical_analyser.acceptIdentifier()
                Type = getIdentType(ident)
                if Type == "boolean":
                        msg = "Cannot read a boolean value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg) 


                lexical_analyser.acceptCharacter(")")

                adr = getTableIndex(ident)

                codeGenerator.code.append(getCall(ident))
                codeGenerator.code.append("get()")

                logger.debug("Call to get "+ident)


        elif lexical_analyser.isKeyword("put"):
                lexical_analyser.acceptKeyword("put")
                lexical_analyser.acceptCharacter("(")

                Type = expression(lexical_analyser)
                if Type == "boolean":
                        msg = "Cannot write a boolean value!"
                        msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                        logger.error(msg)
                        raise AnaSynException(msg) 
                
                lexical_analyser.acceptCharacter(")")

                codeGenerator.code.append(("put()"))

                logger.debug("Call to put")
        else:
                logger.error("Unknown E/S instruction!")
                raise AnaSynException("Unknown E/S instruction!")

def boucle(lexical_analyser):
        logger.debug("parsing while loop: ")
        lexical_analyser.acceptKeyword("while")
        tra = len(codeGenerator.code)
        expreType = expression(lexical_analyser)

        tze = len(codeGenerator.code)
        codeGenerator.code.append(("tze"))

        if expreType != "boolean":
                msg = "Expecting a boolean expression!"
                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                logger.error(msg)
                raise AnaSynException(msg) 

        lexical_analyser.acceptKeyword("loop")

        suiteInstr(lexical_analyser)
        
        codeGenerator.code.append("tra("+str(tra)+")")
        codeGenerator.code[tze] = "tze("+str(codeGenerator.get_instruction_counter())+")"

        lexical_analyser.acceptKeyword("end")

        logger.debug("end of while loop ")

def altern(lexical_analyser):
        logger.debug("parsing if: ")
        lexical_analyser.acceptKeyword("if")

        expreType = expression(lexical_analyser)

        tze = len(codeGenerator.code)
        codeGenerator.code.append(("tze"))

        if expreType != "boolean":
                msg = "Expecting a boolean expression!"
                msg +=" <line "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_line_index())+", column "+str(lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index].get_col_index())+"> !"
                logger.error(msg)
                raise AnaSynException(msg) 

        lexical_analyser.acceptKeyword("then")

        suiteInstr(lexical_analyser)
        
        tra = len(codeGenerator.code)
        codeGenerator.code.append(("tra"))
        codeGenerator.code[tze] = "tze("+str(tra+1)+")"

        if lexical_analyser.isKeyword("else"):
                lexical_analyser.acceptKeyword("else")
                suiteInstr(lexical_analyser)
       
        lexical_analyser.acceptKeyword("end")
        codeGenerator.code[tra] = "tra("+str(codeGenerator.get_instruction_counter())+")"

        logger.debug("end of if")

def retour(lexical_analyser):
        logger.debug("parsing return instruction")
        lexical_analyser.acceptKeyword("return")
        
        expreType = expression(lexical_analyser)
        
        if identifierTable[currentProcedureIndex][1] == "function":
                codeGenerator.code.append("retourFonct()")
        else:
                codeGenerator.code.append("retourProc()")
        return expreType

        

########################################################################
import tkinter
import tkinter.filedialog

#Fonction pour choisir un ficher
def prompt_file(): 
    top = tkinter.Tk()
    top.withdraw()
    file_name = tkinter.filedialog.askopenfilename(parent=top)
    top.destroy()
    print("ouverture du fichier ",file_name)
    return file_name

def main():
        
        parser = argparse.ArgumentParser(description='Do the syntactical analysis of a NNP program.')
        parser.add_argument('inputfile', type=str, nargs=1, help='name of the input source file')
        parser.add_argument('-o', '--outputfile', dest='outputfile', action='store', \
                default="", help='name of the output file (default: stdout)')
        parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')
        parser.add_argument('-d', '--debug', action='store_const', const=logging.DEBUG, \
                default=logging.INFO, help='show debugging info on output')
        parser.add_argument('-p', '--pseudo-code', action='store_const', const=True, default=False, \
                help='enables output of pseudo-code instead of assembly code')
        parser.add_argument('--show-ident-table', action='store_true', \
                help='shows the final identifiers table')
        args = parser.parse_args()

        filename = args.inputfile[0]
        f = None
        try:
                f = open(filename, 'r')
        except:
                print("Error: can\'t open input file!")
                return
                
        outputFilename = args.outputfile
        
        # create logger      
        LOGGING_LEVEL = args.debug
        logger.setLevel(LOGGING_LEVEL)
        ch = logging.StreamHandler()
        ch.setLevel(LOGGING_LEVEL)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)

        if args.pseudo_code:
                True
        else:
                False

        lexical_analyser = analex.LexicalAnalyser()
        
        lineIndex = 0
        for line in f:
                line = line.rstrip('\r\n')
                lexical_analyser.analyse_line(lineIndex, line)
                lineIndex = lineIndex + 1
        f.close()
        
        # launch the analysis of the program
        lexical_analyser.init_analyser()
        program(lexical_analyser)

        if args.show_ident_table:
                        print("------ IDENTIFIER TABLE ------")
                        for ident in identifierTable:
                                print(str(ident[:]))
                        print("------ END OF IDENTIFIER TABLE ------")

        if outputFilename != "":
                        try:
                                        output_file = open(outputFilename, 'w')
                        except:
                                        print("Error: can\'t open output file!")
                                        return
        else:
                        output_file = sys.stdout

        # Outputs the generated code to a file
        instrIndex = 0
        while instrIndex < codeGenerator.get_instruction_counter():
                output_file.write("%s\n" % str(codeGenerator.get_instruction_at_index(instrIndex)))
                instrIndex += 1
                
        if outputFilename != "":
                        output_file.close() 

########################################################################                                 

class codeGenerator():
        code = []

        def get_instruction_counter():
                return len(codeGenerator.code)

        def get_instruction_at_index(instrIndex):
                return codeGenerator.code[instrIndex]
        
if __name__ == "__main__":
        main()

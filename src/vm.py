
import sys, argparse, re
import logging

from copy import deepcopy

code = []
index = 0

#Lecture du fichier
def load(instr): 
        code.append(instr)

pile = []
currentBloc = 0
traStatIndex = []

#######################################
###Instructions
#######################################
def debutProg():
        return None

def finProg():
        quit()

def empiler(val):
        pile.append(val)

def empilerAd(ad):
        pile.append(currentBloc + ad)

def valeurPile():
        pile.append( pile[pile.pop(-1)] )

def affectation():
        val = pile.pop(-1)
        pile[pile.pop(-1)] = val

def ou():
        pile.append( pile.pop(-1) or pile.pop(-1) )

def et():
        pile.append( pile.pop(-1) and pile.pop(-1) )

def non():
        pile.append( not pile.pop(-1) )

def form(val):
        return 1 if val==True else 0 if val==False else val

def egal():
        pile.append( form(pile.pop(-1)) == form(pile.pop(-1)) )#

def diff():
        pile.append( form(pile.pop(-1)) != form(pile.pop(-1)) )#

def inf():
        pile.append( pile.pop(-1) > pile.pop(-1) )

def infeg():
        pile.append( pile.pop(-1) >= pile.pop(-1) )

def sup():
        pile.append( pile.pop(-1) < pile.pop(-1) )

def supeg():
        pile.append( pile.pop(-1) <= pile.pop(-1) )

def add():
        pile.append( pile.pop(-1) + pile.pop(-1) )

def sous():
        pile.append( -pile.pop(-1) + pile.pop(-1) )

def moins():
        pile.append( -pile.pop(-1) )

def mult():
        pile.append( pile.pop(-1) * pile.pop(-1) )

def div():
        pile.append( pile.pop(-2) // pile.pop(-1) )

def valeurPile():
        pile.append( pile[pile.pop(-1)] )

def get():
        pile[pile.pop(-1)] = int(input('> '))

def put():
        print(pile.pop(-1))

def tra(ad):
        global index
        index = ad-1

def tze(ad):
        if not pile.pop(-1):
                tra(ad)

def retourFonct():
        global currentBloc
        ad,nbP = traStatIndex.pop(-1)
        for _ in range(nbP):
                pile.pop(currentBloc)
        currentBloc = pile.pop(currentBloc - 1)
        tra(ad)

def retourProc():
        global currentBloc
        ad,nbP = traStatIndex.pop(-1)
        for _ in range(nbP):
                pile.pop(currentBloc)
        currentBloc = pile.pop(currentBloc - 1)
        tra(ad)

def empilerParam(ad):
        pile.append(pile[currentBloc + ad])

def traStat(ad,nbP):
        global currentBloc
        currentBloc = len(pile) - nbP
        traStatIndex.append([index+1,nbP])
        tra(ad)

def reserverBloc():
        pile.append(currentBloc)

def reserver(n):
        for _ in range(n):
                pile.append("_")
        if currentBloc:
                traStatIndex[-1][1] += n



#Execution du programme
def run(code): 
        global index
        while 1:
                s = code[index]
                eval(code[index])
                index += 1

def main():
        parser = argparse.ArgumentParser()
        parser.add_argument('inputfile', type=str, nargs=1, help='name of the input source file')
        args = parser.parse_args()

        filename = args.inputfile[0]
        f = None
        try:
                f = open(filename, 'r')
        except:
                print("Error: can\'t open input file!")
                return
        
        lineIndex = 0
        for line in f:
                line = line.rstrip('\r\n')
                lineIndex = lineIndex + 1
                load(line)
        f.close()

        run(code)

if __name__ == "__main__":
    main()
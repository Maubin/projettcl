import tkinter
import tkinter.filedialog
from Analyseur_Lexical import *


table = [[],[],[]] #[[ident] , [Op Type] , [ident Type]]

def decla_id(regle,i):
    def f(chaine,cur,ty):
        global table
        correct,err,decal,ty2 = read_expr(regle, chaine, cur, ty)
        if correct:
            #print(cur,regle)
            if i==2 and regle != ():
                while len(table[0])>len(table[2]):
                      table[i].append(chaine[cur][1])

            elif regle == ():
                table[i].append(None)
            else:
                table[i].append(chaine[cur][1])
                
        return (correct,err,decal,ty)
    return f



########################################################################				 	
#### Syntactical Diagrams
########################################################################				 	

Rules = {
'<programme>' : ('<specifProgPrinc>' , 'is' , '<corpsProgPrinc>'),
'<corpsProgPrinc>' : [('<partieDecla>' , 'begin' , '<suiteInstr>' , 'end' , '.') , ('begin' , '<suiteInstr>' , 'end' , '.')],
'<specifProgPrinc>' : (decla_id('procedure',1) , decla_id('<ident>',0),decla_id((),2)),

'<partieDecla>' : [('<listeDeclaOp>' , ['<listeDeclaVar>',()]) , '<listeDeclaVar>'], ##

'<listeDeclaOp>' : [('<declaOp>' , ';' , ['<listeDeclaOp>',()]) , ('<declaOp>' , ';')],## ajout ()
'<declaOp>' : ['<fonction>' , '<procedure>'],
'<procedure>' : (decla_id('procedure',1) , decla_id('<ident>',0) , decla_id((),2) , '<partieFormelle>' , 'is' , '<corpsProc>'),
'<fonction>' : (decla_id('function',1) , decla_id('<ident>',0) , '<partieFormelle>' , 'return' , decla_id('<type>',2) , 'is' , '<corpsFonct>'),

'<corpsProc>' : [('<partieDeclaProc>' , 'begin' , '<suiteInstr>' , 'end') , ('begin' , '<suiteInstr>' , 'end')],
'<corpsFonct>' : [('<partieDeclaProc>' , 'begin' , '<suiteInstrNonVide>' , 'end') , ('begin' , '<suiteInstrNonVide>' , 'end')],

'<partieFormelle>' : [('(' , '<listeSpecifFormelles>' , ')') , ('(',')')],   ## changements '()' ou '(',')'
'<listeSpecifFormelles>' : ('<specif>' , [(';' , '<listeSpecifFormelles>'),()]), ##
'<specif>' : ('<declaListeIdent>' , ':' , ['<mode>',()] , decla_id('<type>',2)),
'<mode>' : [('in' , 'out'),'in' ], ##inversion
'<type>' : ['integer' , 'boolean'],

'<partieDeclaProc>' : '<listeDeclaVar>',
'<listeDeclaVar>' : [('<declaVar>' , ['<listeDeclaVar>',()]) , '<declaVar>'], ## ajout ()
'<declaVar>' : ('<declaListeIdent>' , ':' , decla_id('<type>',2) , ';'),
'<declaListeIdent>' : (decla_id('<ident>',0),decla_id((),1) , [(',' , '<declaListeIdent>'),()]), ## ajout ()

'<listeIdent>' : [('<ident>' , [(',' , '<listeIdent>'),()]) , '<ident>'], ## ajout ()

'<suiteInstrNonVide>' : ('<instr>' , [( ';' , '<suiteInstrNonVide>'),()]), #!; ##
'<suiteInstr>' : ['<suiteInstrNonVide>' , ()],
'<instr>' : ['<affectation>' , '<boucle>' , '<altern>' , '<es>' , '<retour>' , '<appelProc>'],
'<appelProc>' : [('<ident>' , '(' , '<listePe>' , ')') , ('<ident>' , '(' , ')')],
'<listePe>' : [('<expression>' , ',' , '<listePe>') , '<expression>'], ##s
'<affectation>' : ('<ident>' , ':=' , '<expression>'),
#'<expression>' : [infixe('<exp1>' , 'or' , '<expression>') , '<exp1>'] ,##ajouter un fonction d'evaluation

'<expression>' : ['<exp0>'] ,

'<expBool>' : [ '<exp0>','<exp1>'],#
'<exp0>' : [( '<prim>' , 'or' , '<expBool>'),  '<exp1>' ] ,
'<exp1>' : [( '<prim>' , 'and' , '<expBool>'),  '<exp2>' ] ,
'<exp2>' : [( '<prim>' , '<opRel>' , '<exp2>'), '<exp3>' ] ,
'<opRel>' : ['=' , '/=' , '<' , '<=' , '>' , '>='],

'<expInt>' : [ '<exp3>','<exp4>'], #
'<exp3>' : [('<prim>' , '<opAd>' , '<exp0>'), '<exp4>'] ,##
'<opAd>' : ['+' , '-'],
'<exp4>' : [('<prim>' , '<opMult>' , '<exp0>'), '<prim>'] ,##
'<opMult>' : ['*' , '/'],

'<prim>' : ['<expUnaire>' , '<elemPrim>'],# '<expression>'], ##
'<expUnaire>' : [(['+' , '-'] , '<elemPrim>'),('not', '<elemPrim>')],## #not
'<opUnaire>' : ['+' , '-' , 'not'], ##ajouter verif '+-, ++' exp unaire

'<elemPrim>' : ['<valeur>' , ('(' , '<expression>' , ')') , '<appelFonct>' , ('<ident>')], ##inversion
'<appelFonct>' : [('<ident>' , '(' , '<listePe>' , ')') , ('<ident>' , '(' , ')')],

'<valeur>' : [('<entier>') , ('<valBool>')],
'<valBool>' : ['true' , 'false'],
'<es>' : [('get' , '(' , '<ident>' , ')') , ('put' , '(' , '<expression>' , ')')],
'<boucle>' : ('while' , '<expression>' , 'loop' , '<suiteInstr>' , 'end'),
'<altern>' : [('if' , '<expression>' , 'then' , '<suiteInstr>' , 'end') , ('if' , '<expression>' , 'then' , '<suiteInstr>' , 'else' , '<suiteInstr>' , 'end')],
'<retour>' : ('return' , '<expression>'),

#'<ident>' : [('<lettre>' , '<listeLettreOuChiffre>') , '<lettre>'],
'<listeLettreOuChiffre>' : [('<lettreOuChiffre>' , '<listeLettreOuChiffre>') , '<lettreOuChiffre>'],
'<lettreOuChiffre>' : ['<lettre>' , '<chiffre>'],

'<lettre>' : [c for c in"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"],
'<chiffre>': [c for c in'0123456789'],
#'<entier>' : [('<chiffre>' , '<entier>') , '<chiffre>'],
}
########################################################################


curMax = 0
error = ''
def read_expr(regle,chaine,cur,ty):
    global error,curMax
    #print(chaine[cur][1],regle)
    
    if type(regle) == type(read_expr):
        return regle(chaine,cur,ty)
    
    if type(regle) == list:
        for regle_i in regle:
            correct,err,decal,ty2 = read_expr(regle_i, chaine, cur, ty)
            if correct:
                return (True,err,decal,ty)
        return (False,err,0,ty)
            
    elif type(regle) == tuple:
        for i in range(len(regle)):
            err0 = None
            correct,err,decal,ty2 = read_expr(regle[i], chaine, cur, ty)
            cur = decal
            if err0 is None:
                err0 = err
            if not correct:
                return (False,err0,0,ty)
        return (True,0,cur,ty)

    elif cur > len(chaine):
         return (False,"Erreur : pas de fin de programme",0,ty)
    
    elif regle in Rules:

        return read_expr(Rules[regle], chaine, cur ,ty)

    e0,e1 = chaine[cur]
    if e1 == regle or (regle,e0) in [('<ident>',"Identifier"),('<entier>',"Integer")]:

        return (True,0,cur+1,ty)

    err = "Erreur: '"+str(e1)+"'  Elément attendu: '"+ Type(str(regle))+"'" + regle
    if cur>curMax: ##faire la liste des erreurs
        error = err
        curMax = cur
    return (False,err,0,ty)


def main():

    #chaine = reformat(analyseur("programme test.nno"))
    chaine = analyseur("programme test.nno")
    #chaine = analyseur(prompt_file()) 
    #print(chaine)

    
    cmp = read_expr("<programme>",chaine,0,None)

    
    if not cmp[0]:
        print(error)
    else:
        print(cmp)

    print("------ IDENTIFIER TABLE ------")
    print(table)
    print("------ END OF IDENTIFIER TABLE ------")

    #print(chaine)

if __name__ == "__main__":
    main() 


        


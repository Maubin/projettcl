import tkinter
import tkinter.filedialog

#Fonction pour choisir un ficher
def prompt_file(): 
    top = tkinter.Tk()
    top.withdraw()
    file_name = tkinter.filedialog.askopenfilename(parent=top)
    top.destroy()
    print("ouverture du fichier ",file_name)
    return file_name

def save_to_file(chaine, filename):
    output_file = None
    if filename != "":
        try:
            output_file = open(filename, 'w')
        except:
            output_file = sys.stdout
        for c in chaine:
            output_file.write((' '*12)[:-len(c[0])] + c[0] +' : '+ c[1] + '\n')
        output_file.close()


#symboles de longueur 1
Tokens1 = ['.',"?","!",
      ',', ':', ';', '/', "(", ")", '[', ']',
      '1','2','3','4','5','6','7','8','9','0',
      '=', '+', '*', '%', '<', "-",
      ]

#symboles de longueur 2
Tokens2 = [':=', '==', '/=', '<=', '>=']

#lettres des symboles de longueur quelconques
Chars  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZéèîôâÉ"

#Commentaires et autres
Dump = ['//']

#espaces 
Spaces = [' ', '\n']

#Séparation des symboles
def split(t):
    l = ['']
    for c in t:
        if c in Spaces:
            if len(l[-1]):
                l.append('')
        elif c not in Chars:#in W1:
            if len(l[-1]):
                l.append('')
            l[-1] += c
            l.append('')
        else:
            if 64<ord(c)<91:
                c = chr(ord(c)+32)
            l[-1] += c
 
    return l if len(l[-1]) else l[:-1]

def Type(valeur):
    Type = ["Keyword", "Character", "Type", "Symbol"]
    liste = [
        ["procedure","is","begin",":=", "get", "end","put","while","loop","for","to","if","else","then",
         "function","in","out","true","false",],
        [",", ":",";",".", "(", ")", '[', ']',],
        ["boolean","integer",],
        ['<','>',"<=",">=","=","/=","and","or","not","+","-","*","/","%",],
        ]
    for i in range(len(liste)):
        if valeur in liste[i]:
            return Type[i]
        
    if valeur[0].isdigit():
        return "Integer"
    
    else:
        return "Identifier"
    
def analyseur(file):
    fichier = open(file)
    chaine = []
    for ligne in fichier.readlines():
        ligne = ligne.strip() #Retire le retours à la ligne
        mots = split(ligne) 
        i = 0
        while i < len(mots):
            m = mots[i]

            if m.isdigit():
                while i+1<len(mots) and mots[i+1].isdigit():
                    m = m + mots[i+1]
                    i += 1
                    
            elif i+1<len(mots):
                m2 = mots[i]+mots[i+1]

                if m2 in Dump:
                    break
                elif m2 in Tokens2:
                    m = m2
                    i += 1
            i+=1
            chaine.append((Type(m),m))

    return chaine


#tests
if __name__ == "__main__":
    file = "C:/Users/etienne/Desktop/TCP/projettcl/tests/nna/correct3.nno"
    file = prompt_file()
    chaine = analyseur(file)
    for c in chaine:
        print(c[0],':',c[1])
    save_to_file(chaine,'analyse lexicale.txt')
